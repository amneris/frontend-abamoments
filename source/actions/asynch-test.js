// TODO split this file!!!!

import btoa from 'btoa'; // polyfill required to run unit tests

import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import ApiRewire from '../services/api';
import AsyncRewire from './asynch';

import * as Async from './asynch';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const MOCKED_URL = 'http://mocked-url';
const CLIENT_ID = 'clientId';
const CLIENT_SECRET = 'clientSecret';

const mockedLoginDataUsingPassword = {
  autoLoginKey: undefined,
  email: 'email@test.com',
  password: 'testPassword'
};

const mockedLoginDataUsingAutoLoginKey = {
  autoLoginKey: 'testAutoLoginKey',
  email: 'email@test.com',
  password: undefined
};

const mockedUserData = {
  country: 'ES',
  name: 'userName',
  surname: 'userSurname',
  userType: 'free'
};

const mockedTokenData = {
  accessToken: 'testToken',
  client: {},
  data: {
    access_token: 'testToken',
    expires_in: 60,
    jti: '00000000-0000-0000-0000-000000000000',
    refresh_token: 'testRefreshToken',
    scope: 'read write trust',
    token_type: 'bearer',
    userId: '1',
  },
  expires: 'Mon Jan 01 2030 09:01:00 GMT+0200 (CEST)',
  refreshToken: 'testRefreshToken',
  tokenType: 'bearer'
};

// mock services url
AsyncRewire.__Rewire__('AppConfig', {
  getApiUrl: () => MOCKED_URL,
  getCampusUrl: () => MOCKED_URL,
  getOauth2ClientId: () => CLIENT_ID,
  getOauth2ClientSecret: () => CLIENT_SECRET,
});

afterEach(() => {
  nock.cleanAll();
});

/* ********************************************************************************************************* */
/* **** USER AUTHENTICATION ******************************************************************************** */
/* ********************************************************************************************************* */

describe ('USER AUTHENTICATION', () => {

  /* callActionFetchLogin function is a TEMPORARY SOLUTION until we have a login module
  * this function can receive the login data from the parameters (minimum combo of email + auto login key OR email + password)
  * or can take this data from the campus session in the browser (using an optional auto login key)
  * it should dispatch the SET_USER action in order to store the minimum user data to be authenticated when calling
  * backend micro-services
  * todo: remove this function when we have a login module
  * */
  describe ('callActionFetchLogin function', () => {

    it ('dispatches SET_USER when the login data (email and autoLoginKey) has been fetched from abawebapps', (done) => {

      const store = mockStore({});

      const expectedActions = [
        {
          type: 'SET_USER',
          user: mockedLoginDataUsingAutoLoginKey
        }
      ];

      const urlParams = {};

      nock(MOCKED_URL)
        .get('/payments/wsGetUserData')
        .reply(200, mockedLoginDataUsingAutoLoginKey);

      store.dispatch(Async.callActionFetchLogin(urlParams))
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions);
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('dispatches SET_USER when the login data (email and password) has been fetched from abawebapps', (done) => {

      const store = mockStore({});

      const expectedActions = [
        {
          type: 'SET_USER',
          user: mockedLoginDataUsingPassword
        }
      ];

      const urlParams = {};

      nock(MOCKED_URL)
        .get('/payments/wsGetUserData')
        .reply(200, mockedLoginDataUsingPassword);

      store.dispatch(Async.callActionFetchLogin(urlParams))
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('dispatches SET_USER when the login data (email and password) has been fetched from abawebapps using autoLoginKey', (done) => {

      const store = mockStore({});

      const expectedActions = [
        {
          type: 'SET_USER',
          user: mockedLoginDataUsingPassword
        }
      ];

      const urlParams = { autol: mockedLoginDataUsingAutoLoginKey.autoLoginKey };

      nock(MOCKED_URL)
        .get('/payments/wsGetUserData')
        .query(urlParams)
        .reply(200, mockedLoginDataUsingPassword);

      store.dispatch(Async.callActionFetchLogin(urlParams))
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('dispatches FORBID_ACCESS when the login data could not be fetched from abawebapps', (done) => {

      const store = mockStore({});

      const expectedActions = [
        {
          type: 'FORBID_ACCESS'
        }
      ];

      const urlParams = {};

      nock(MOCKED_URL)
        .get('/payments/wsGetUserData')
        .reply(401);

      store.dispatch(Async.callActionFetchLogin(urlParams))
        .catch(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('dispatches SET_USER when it is called without any parameter', (done) => {

      const store = mockStore({});

      const expectedActions = [
        {
          type: 'SET_USER',
          user: mockedLoginDataUsingAutoLoginKey
        }
      ];

      nock(MOCKED_URL)
        .get('/payments/wsGetUserData')
        .reply(200, mockedLoginDataUsingAutoLoginKey);

      store.dispatch(Async.callActionFetchLogin())
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('dispatches SET_USER when the login data (email and autoLoginKey) has been taken from the parameters', (done) => {

      const store = mockStore({});

      const expectedActions = [
        {
          type: 'SET_USER',
          user: mockedLoginDataUsingAutoLoginKey
        }
      ];

      const urlParams = { email: encodeURIComponent(mockedLoginDataUsingAutoLoginKey.email), autol: mockedLoginDataUsingAutoLoginKey.autoLoginKey };

      store.dispatch(Async.callActionFetchLogin(urlParams))
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('dispatches SET_USER when the login data (email and password) has been taken from the parameters', (done) => {

      const store = mockStore({});

      const expectedActions = [
        {
          type: 'SET_USER',
          user: mockedLoginDataUsingPassword
        }
      ];

      const urlParams = { email: encodeURIComponent(mockedLoginDataUsingPassword.email), password: mockedLoginDataUsingPassword.password };

      store.dispatch(Async.callActionFetchLogin(urlParams))
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
        .then(done, done); // callback to say that the promise has finished
    });

  });

  /* fetchAuthTokenByAutoLoginKey function is a TEMPORARY SOLUTION until we have a login module
   * this function takes the user object from the store expecting an email and auto login key
   * to be able to request a token to the authorization service
   * it should dispatch SET_API_AUTH to store the token data and SET_USER to store the user id received
   * todo: remove this function when we have a login module
   * */
  describe ('fetchAuthTokenByAutoLoginKey function', () => {

    it ('dispatches SET_API_AUTH and SET_USER when the token has been fetched', (done) => {

      const authUrlPath = '/oauth/token';

      nock(MOCKED_URL, {
        headers: {
          'Authorization': 'Basic '+ btoa(CLIENT_ID + ':' + CLIENT_SECRET),
          'Content-Type':'application/x-www-form-urlencoded'
        }
      })
        .post(authUrlPath, {
          grant_type: 'token',
          password: mockedLoginDataUsingAutoLoginKey.autoLoginKey,
          username: mockedLoginDataUsingAutoLoginKey.email
        })
        .reply(200, mockedTokenData.data);

      const store = mockStore();

      const expectedActions = [
        {
          type: 'SET_API_AUTH',
          authToken: mockedTokenData
        },
        {
          type: 'SET_USER',
          user: {
            id: mockedTokenData.data.userId
          }
        }
      ];

      ApiRewire.__Rewire__('ClientOAuth2CreateToken', () => mockedTokenData );

      store.dispatch(Async.fetchAuthTokenByAutoLoginKey(MOCKED_URL + authUrlPath, mockedLoginDataUsingAutoLoginKey.email, mockedLoginDataUsingAutoLoginKey.autoLoginKey))
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions);

          ApiRewire.__ResetDependency__('ClientOAuth2CreateToken');
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('dispatches FORBID_ACCESS when the token could not be fetched', (done) => {

      const authUrlPath = '/oauth/token';
      const invalidData = {
        grant_type: 'token',
        password: 'invalidAutoLoginKey',
        username: 'invalidUsername'
      };

      nock(MOCKED_URL, {
        headers: {
          'Authorization': 'Basic '+ btoa(CLIENT_ID + ':' + CLIENT_SECRET),
          'Content-Type':'application/x-www-form-urlencoded'
        }
      })
        .post(authUrlPath, invalidData)
        .reply(401);

      const store = mockStore();

      const expectedActions = [
        {
          type: 'FORBID_ACCESS'
        }
      ];

      store.dispatch(Async.fetchAuthTokenByAutoLoginKey(MOCKED_URL + authUrlPath, invalidData.username, invalidData.password))
        .catch(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
        .then(done, done); // callback to say that the promise has finished
    });

  });

  /* fetchAuthTokenByPassword function takes the user object from the store expecting an email and a password
   * to be able to request a token to the authorization service
   * it should dispatch SET_API_AUTH to store the token data and SET_USER to store the user id received
   * todo: in the future, this function should take the data from a login module instead of the store
   * */
  describe ('fetchAuthTokenByPassword function', () => {

    it ('dispatches SET_API_AUTH and SET_USER when the token has been fetched', (done) => {

      const authUrlPath = '/oauth/token';

      nock(MOCKED_URL, {
        headers: {
          'Authorization': 'Basic '+ btoa(CLIENT_ID + ':' + CLIENT_SECRET),
          'Content-Type':'application/x-www-form-urlencoded'
        }
      })
        .post(authUrlPath, {
          grant_type: 'password',
          password: mockedLoginDataUsingPassword.password,
          username: mockedLoginDataUsingPassword.email
        })
        .reply(200, mockedTokenData.data);

      const store = mockStore();

      const expectedActions = [
        {
          type: 'SET_API_AUTH',
          authToken: mockedTokenData
        },
        {
          type: 'SET_USER',
          user: {
            id: mockedTokenData.data.userId
          }
        }
      ];

      ApiRewire.__Rewire__('ClientOAuth2GetToken', () => mockedTokenData );

      store.dispatch(Async.fetchAuthTokenByPassword(MOCKED_URL + authUrlPath, mockedLoginDataUsingPassword.email, mockedLoginDataUsingPassword.password))
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions);

          ApiRewire.__ResetDependency__('ClientOAuth2GetToken');
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('dispatches FORBID_ACCESS when the token could not be fetched', (done) => {

      const authUrlPath = '/oauth/token';
      const invalidData = {
        grant_type: 'password',
        password: 'invalidPassword',
        username: 'invalidUsername'
      };

      nock(MOCKED_URL, {
        headers: {
          'Authorization': 'Basic '+ btoa(CLIENT_ID + ':' + CLIENT_SECRET),
          'Content-Type':'application/x-www-form-urlencoded'
        }
      })
        .post(authUrlPath, invalidData)
        .reply(401);

      const store = mockStore();

      const expectedActions = [
        {
          type: 'FORBID_ACCESS'
        }
      ];

      store.dispatch(Async.fetchAuthTokenByPassword(MOCKED_URL + authUrlPath, invalidData.username, invalidData.password))
        .catch(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
        .then(done, done); // callback to say that the promise has finished
    });

  });

  /* fetchAuthToken function takes the user object from the store expecting an email and a password or an auto login key
   * to be able to request a token to the authorization service
   * it should choose between fetching the token with auto login key or password depending on the stored data
   * */
  describe ('fetchAuthToken function', () => {

    it ('calls function fetchAuthTokenByAutoLoginKey when there is an auto login key in the store', (done) => {
      const store = mockStore({ user: mockedLoginDataUsingAutoLoginKey });

      AsyncRewire.__Rewire__('fetchAuthTokenByAutoLoginKey', () => dispatch => Promise.resolve('function has been called'));

      store.dispatch(Async.fetchAuthToken())
        .then((result) => {
          expect(result).toEqual('function has been called');

          AsyncRewire.__ResetDependency__('fetchAuthTokenByAutoLoginKey');
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('calls function fetchAuthTokenByPassword when there is a password in the store', (done) => {
      const store = mockStore({ user: mockedLoginDataUsingPassword });

      AsyncRewire.__Rewire__('fetchAuthTokenByPassword', () => dispatch => Promise.resolve('function has been called'));

      store.dispatch(Async.fetchAuthToken())
        .then((result) => {
          expect(result).toEqual('function has been called');
        })
        .then(done, done); // callback to say that the promise has finished
    });
  });

  /* handleAuthToken function returns a valid token even if
   * 1) needs to be created
   * 2) is expired
   * */
  describe ('handleAuthToken function', () => {

    it ('calls function fetchAuthToken when there is no authorization token in the store', (done) => {
      const store = mockStore({ apiAuth: {} });

      AsyncRewire.__Rewire__('fetchAuthToken', () => dispatch => Promise.resolve('function has been called'));

      store.dispatch(Async.handleAuthToken())
        .then((result) => {
          expect(result).toEqual('function has been called');

          AsyncRewire.__ResetDependency__('fetchAuthToken');
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('calls function refreshToken when the authorization token in the store has been expired', (done) => {
      const store = mockStore({ apiAuth: { authToken: mockedTokenData }, user: mockedLoginDataUsingAutoLoginKey });

      AsyncRewire.__Rewire__('ClientOAuth2HasExpired', () => true);
      AsyncRewire.__Rewire__('refreshToken', () => dispatch => Promise.resolve('function has been called'));

      store.dispatch(Async.handleAuthToken())
        .then((result) => {
          expect(result).toEqual('function has been called');

          AsyncRewire.__ResetDependency__('ClientOAuth2HasExpired');
          AsyncRewire.__ResetDependency__('refreshToken');
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('returns the token in the store if it is still valid', (done) => {
      const store = mockStore({ apiAuth: { authToken: mockedTokenData } });

      AsyncRewire.__Rewire__('ClientOAuth2HasExpired', () => false);

      store.dispatch(Async.handleAuthToken())
        .then((result) => {
          expect(result).toEqual(mockedTokenData);

          AsyncRewire.__ResetDependency__('ClientOAuth2HasExpired');
        })
        .then(done, done); // callback to say that the promise has finished
    });
  });

  /* refreshToken function refreshes the token using the refresh token
   * or creates a new token when the refresh token has also expired
   * */
  /*describe ('refreshToken function', () => {

    it ('dispatches SET_API_AUTH when the token has been refreshed', (done) => {
      const store = mockStore({ apiAuth: {}, user: mockedLoginDataUsingAutoLoginKey });

      AsyncRewire.__Rewire__('ClientOAuth2RefreshToken', () => Promise.resolve(mockedTokenData));

      const expectedActions = [
        {
          type: 'SET_API_AUTH',
          authToken: mockedTokenData
        }
      ];

      store.dispatch(Async.refreshToken())
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions);

          AsyncRewire.__ResetDependency__('ClientOAuth2RefreshToken');
        })
        .then(done, done); // callback to say that the promise has finished
    });

    it ('calls function fetchAuthToken when there refresh token has also been expired', (done) => {
      const store = mockStore({ apiAuth: {} });

      AsyncRewire.__Rewire__('ClientOAuth2RefreshToken', () => { throw Error() });
      AsyncRewire.__Rewire__('fetchAuthToken', () => dispatch => Promise.resolve('function has been called'));

      store.dispatch(Async.refreshToken())
        .catch((result) => {
          expect(result).toEqual('function has been called');

          AsyncRewire.__ResetDependency__('ClientOAuth2RefreshToken');
          AsyncRewire.__ResetDependency__('fetchAuthToken');
        })
        .then(done, done); // callback to say that the promise has finished
    });
  });*/

});

/* ********************************************************************************************************* */
/* **** USER DATA ****************************************************************************************** */
/* ********************************************************************************************************* */
/*
describe ('USER DATA', () => {

  describe ('get data of the authenticated user', () => {

    it ('dispatches SET_USER when the data has been fetched', () => {

      nock(MOCKED_URL)
        .get('/api/v1/users/265')
        .reply(200, mockedUserData);

      const store = mockStore({ apiAuth: mockedTokenData, user: mockedLoginDataUsingAutoLoginKey });

      const expectedActions = [
        {
          type: 'SET_USER',
          user: mockedUserData
        }];

      store.dispatch(Async.callActionFetchUserInfo())
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
    });
  });
});*/