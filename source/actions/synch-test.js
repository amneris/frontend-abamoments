import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Synch, {
  callActionSetAppState
} from './synch';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


describe('sync actions', () => {

  describe('set app state', () => {
    it('should create SET_APP_STATE when the action is called', () => {

      const actionData = {title: 'some_title', language: 'some_language'};
      const expectedActions = [
        {
          type: 'SET_APP_STATE',
          appState: actionData
        }
      ];

      const store = mockStore({});

      store.dispatch(callActionSetAppState(actionData));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

});
