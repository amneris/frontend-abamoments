// Navigation actions
export const FORBID_ACCESS = 'FORBID_ACCESS';
export const GO_TO_DASHBOARD = 'GO_TO_DASHBOARD';
export const GO_TO_MOMENT = 'GO_TO_MOMENT';
export const GO_TO_COURSE = 'GO_TO_COURSE';
export const VIEW_DASHBOARD = 'VIEW_DASHBOARD';
export const VIEW_MOMENT = 'VIEW_MOMENT';
export const START_FINISH_MOMENT = 'START_FINISH_MOMENT';
export const END_FINISH_MOMENT = 'END_FINISH_MOMENT';
export const CLOSE_MOMENT = 'CLOSE_MOMENT';
export const START_MOMENT_EXERCISE = 'START_MOMENT_EXERCISE';
export const SELECT_MOMENT_ANSWER = 'SELECT_MOMENT_ANSWER';

// Asynchronous data actions
export const SET_USER = 'SET_USER';
export const SET_PROGRESS_SAVED = 'SET_PROGRESS_SAVED';

// Application actions
export const SET_APP_STATE = 'SET_APP_STATE';
export const SET_URL_PARAMS = 'SET_URL_PARAMS';
export const SET_API_AUTH = 'SET_API_AUTH';

// Course actions
export const SET_TEACHER = 'SET_TEACHER';
export const SET_NEXT_UNIT = 'SET_NEXT_UNIT';
