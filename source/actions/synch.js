import * as actionTypes from './actionTypes';

import {
  setAppState, setUrlParams, goToPage, viewPage,
} from './actionCreators';

export function callActionSetAppState(appState) {
  return setAppState(appState);
}

export function callActionSetUrlParams(urlParams) {
  return setUrlParams(urlParams);
}

export function callActionViewPage() {
  return viewPage();
}

export function callActionGoToDashboard() {
  return {
    type: actionTypes.GO_TO_DASHBOARD
  };
}

export function callActionViewDashboard(momentType) {
  return {
    type: actionTypes.VIEW_DASHBOARD,
    trackerData: {
      cooladata: {
        eventName: 'OPENED_MOMENT_TYPE',
        properties: {
          moment_type: momentType
        }
      }
    }
  };
}

export function callActionGoToMoment(moment) {
  return {
    type: actionTypes.GO_TO_MOMENT,
    moment: {
      audio: moment.audio,
      id: moment.id,
      name: moment.name,
      englishName: moment.englishName,
      status: moment.status.toLowerCase(),
      type: moment.type
    },
    trackerData: {
      cooladata: {
        eventName: 'OPENED_MOMENT',
        properties: {
          moment_id: moment.id,
          moment_type: moment.type
        }
      }
    }
  };
}

export function callActionStartMoment(id, type) {
  return {
    type: actionTypes.VIEW_MOMENT,
    trackerData: {
      cooladata: {
        eventName: 'STARTED_MOMENT',
        properties: {
          moment_id: id,
          moment_type: type
        }
      }
    }
  };
}

export function callActionStartFinishMoment(id, type) {
  return {
    type: actionTypes.START_FINISH_MOMENT,
    trackerData: {
      cooladata: {
        eventName: 'FINISHED_MOMENT',
        properties: {
          moment_id: id,
          moment_type: type
        }
      }
    }
  };
}

export function callActionEndFinishMoment() {
  return {
    type: actionTypes.END_FINISH_MOMENT
  };
}

export function callActionCloseMoment(id, type) {
  return {
    type: actionTypes.CLOSE_MOMENT,
    trackerData: {
      cooladata: {
        eventName: 'CLOSED_MOMENT',
        properties: {
          moment_id: id,
          moment_type: type
        }
      }
    }
  };
}

export function callActionStartMomentExercise(momentId, momentType, exerciseId, exerciseType) {
  return {
    type: actionTypes.START_MOMENT_EXERCISE,
    trackerData: {
      cooladata: {
        eventName: 'STARTED_MOMENT_EXERCISE',
        properties: {
          moment_id: momentId,
          moment_type: momentType,
          exercise_id: exerciseId,
          exercise_type: exerciseType
        }
      }
    }
  };
}

export function callActionSelectMomentAnswer(momentId, momentType, exerciseId, exerciseType, answerId, isCorrect) {
  return {
    type: actionTypes.SELECT_MOMENT_ANSWER,
    trackerData: {
      cooladata: {
        eventName: 'SELECTED_MOMENT_ANSWER',
        properties: {
          moment_id: momentId,
          moment_type: momentType,
          exercise_id: exerciseId,
          exercise_type: exerciseType,
          answer_id: answerId,
          result: isCorrect ? 'CORRECT' : 'INCORRECT'
        }
      }
    }
  };
}

export function callActionGoToCourse(unitId) {
  return {
    type: actionTypes.GO_TO_COURSE,
    unitId
  };
}
