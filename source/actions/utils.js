import QueryString from 'query-string';

export function shouldFetchUserSession(state) {
  const userSession = state.userSession;

  if (!userSession || !userSession.id) {
    return true;
  }

  if (userSession.isFetching) {
    return false;
  }

  return false;
}

export function appendParamsToUrl(url, params) {
  if (params instanceof Object) {
    url += '?' + QueryString.stringify(params);
  }

  return url;
}
