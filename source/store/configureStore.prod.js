import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

import TrackerConfig from '../config/trackerConfig';

import rootReducer from '../reducers';
import urlManager from '../redux/middleware/urlManager';
import eventTracker from '../redux/middleware/eventTracker';
import AbaTrackerSubscriber from 'aba-tracker-subscriber';

export default function configureStore(initialState) {

  AbaTrackerSubscriber.init(TrackerConfig.getTrackersSetup());

  let middleware = [thunkMiddleware];
  
  if (typeof(window) !== 'undefined') {
    middleware = [ ...middleware, urlManager, eventTracker(AbaTrackerSubscriber) ]
  }

  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middleware),
  );
}
