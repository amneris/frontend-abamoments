import React from 'react';

export default class TextItem extends React.Component {

  render() {

    return (
      <div className="textItem-container">
        {this.props.text}
      </div>
    );
  }
}

TextItem.propTypes = {
  text: React.PropTypes.string.isRequired
}
