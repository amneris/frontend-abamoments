import React from 'react';

import AppConfig from '../../config/appConfig';

import BubbleNotification from '../common/bubbleNotification';
import TextItem from './textItem';
import ImageItem from './imageItem';

export default class Answer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isActive: undefined
    };
  }

  componentWillMount() {
    this.setState({
      isActive: true
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isAnsweredCorrectly && !this.props.isAnsweredCorrectly) {
      this.setState({
        isActive: false
      });
    }
  }

  handleClick(e) {
    if (this.state.isActive) {
      this.setState({
        isActive: false
      });

      this.props.onClickCallback(this.props.id, this.props.isCorrect);
    }
  }

  render() {

    return (
      <div className={`answer-container itemType-${this.props.itemType} isActive-${this.state.isActive} ${!this.state.isActive ? 'isCorrect-' + this.props.isCorrect : ''}`}
           onClick={this.handleClick.bind(this)}>

        <div className="item-container">
          {(() => {
            // if is the correct answer and it was just selected
            if (!this.state.isActive && this.props.isCorrect) {
              return <BubbleNotification type='done'/>
            }
          })()}

          {(() => {
            switch (this.props.itemType) {
              case 'text':
                return <TextItem text={this.props.content} />

              case 'image':
                return <ImageItem imagePath={`${AppConfig.getImageAssetsUrl()}/${this.props.momentId}/`}
                                  imageName={this.props.content} />
            }
          })()}
        </div>

      </div>
    );
  }
}

Answer.propTypes = {
  content: React.PropTypes.string.isRequired,
  id: React.PropTypes.string.isRequired,
  itemType: React.PropTypes.oneOf(['text', 'image']).isRequired,
  isAnsweredCorrectly: React.PropTypes.bool,
  isCorrect: React.PropTypes.bool.isRequired,
  momentId: React.PropTypes.string.isRequired,
  onClickCallback: React.PropTypes.func.isRequired
}
