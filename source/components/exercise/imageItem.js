import React from 'react';

import AppConfig from '../../config/appConfig';

export default class ImageItem extends React.Component {

  constructor(props) {
    super(props);

    this.className = `externalImage-${this.props.imageName}`;

    if (typeof window !== 'undefined' && document.getElementById(this.className) == null) {
      let sheet = document.createElement('style');

      sheet.id = this.className;
      sheet.innerHTML = `.${this.className} {
        background-image: url('${this.props.imagePath}${this.props.imageName}www.png');
      }
      @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
        .${this.className} {
          background-image: url('${this.props.imagePath}${this.props.imageName}www@2x.png');
        }
      }`;

      document.body.appendChild(sheet);
    }
  }

  render() {

    return (
      <div className={`imageItem-container ${this.className}`} />
    );
  }
}

ImageItem.propTypes = {
  imageName: React.PropTypes.string.isRequired,
  imagePath: React.PropTypes.string.isRequired
}
