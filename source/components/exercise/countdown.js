import React from 'react';

import AudioPlayer from '../common/audioPlayer';

export default class Countdown extends React.Component {

  constructor(props) {
    super(props);

    this.interval = undefined;
    this.initialCountdown = 3;

    this.state = {
      isFinished: undefined,
      seconds: undefined,
      shouldPlayStart: undefined,
      text: undefined
    };
  }

  componentWillMount() {
    this.setState({
      isFinished: false,
      seconds: this.initialCountdown,
      shouldPlayStart: false,
      text: this.initialCountdown.toString()
    });
  }

  componentDidMount() {
    this.interval = setInterval(::this.updateCountdown, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  updateCountdown() {

    if (this.state.seconds > 1) {
      let seconds = this.state.seconds - 1;

      this.setState({
        seconds: seconds,
        text: seconds.toString()
      });
    }
    else {
      clearInterval(this.interval);

      this.showMomentName();
    }
  }

  showMomentName() {
    this.setState({
      seconds: 0,
      shouldPlayStart: true,
      text: this.props.momentName
    });
  }

  handlePlayEnded() {
    this.props.onFinishCallback();
  }

  render() {

    return (
      <div className="countdown">
        <AudioPlayer source={this.props.momentAudio}
                     shouldPlayStart={this.state.shouldPlayStart}
                     onPlayEndedCallback={::this.handlePlayEnded} />

        <div className={`countdown-text countdown-seconds-${this.state.seconds}`}>{ this.state.text }</div>
      </div>
    );
  }
}

Countdown.propTypes = {
  momentName: React.PropTypes.string.isRequired,
  momentAudio: React.PropTypes.string.isRequired,
  onFinishCallback: React.PropTypes.func.isRequired
}

