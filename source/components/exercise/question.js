import React from 'react';

import AppConfig from '../../config/appConfig';

import TextItem from './textItem';
import ImageItem from './imageItem';
import AudioPlayer from '../common/audioPlayer';

export default class Question extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      shouldPlayStart: undefined,
      isPlaying: undefined
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.lastAnswerClicked !== this.props.lastAnswerClicked) {
      this.setState({
        shouldPlayStart: true
      });
    }
  }

  handlePlay() {
    this.setState({
      isPlaying: true
    });
  }

  handlePlayEnded() {
    this.setState({
      shouldPlayStart: false,
      isPlaying: false
    });

    if (this.props.isAnsweredCorrectly) {
      this.props.onFinishExerciseCallback();
    }
  }

  render() {

    return (
      <div ref={(ref) => { this.container = ref; }}
           className={`question-container isAnsweredCorrectly-${this.props.isAnsweredCorrectly} isPlaying-${this.state.isPlaying}`}>

        <AudioPlayer source={`${AppConfig.getAudioAssetsUrl()}/${this.props.momentId}/${this.props.audio}.mp3`}
                     shouldPlayStart={this.state.shouldPlayStart && this.props.isAnsweredCorrectly}
                     onPlayCallback={::this.handlePlay}
                     onPlayEndedCallback={::this.handlePlayEnded} />

        <AudioPlayer source={'/assets/audio/error.mp3'}
                     shouldPlayStart={this.state.shouldPlayStart && !this.props.isAnsweredCorrectly}
                     onPlayCallback={::this.handlePlay}
                     onPlayEndedCallback={::this.handlePlayEnded} />

        <div className="item-container">
          {(() => {
            switch(this.props.itemType) {
              case 'text':
                return <TextItem text={this.props.content} />

              case 'image':
                return <ImageItem imagePath={`${AppConfig.getImageAssetsUrl()}/${this.props.momentId}/`}
                                  imageName={this.props.content} />
            }
          })()}
        </div>

      </div>
    );
  }
}

Question.propTypes = {
  audio: React.PropTypes.string.isRequired,
  content: React.PropTypes.string.isRequired,
  itemType: React.PropTypes.oneOf(['text', 'image']).isRequired,
  isAnsweredCorrectly: React.PropTypes.bool,
  lastAnswerClicked: React.PropTypes.string,
  momentId: React.PropTypes.string.isRequired,
  onFinishExerciseCallback: React.PropTypes.func.isRequired
}
