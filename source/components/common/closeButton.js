import React from 'react';

export default class CloseButton extends React.Component {

  render() {

    return (
      <div className="closeButton-container base-icon-ui-outline-1_circle-remove"
           onClick={this.props.onClickCallback} />
    );
  }
}

CloseButton.propTypes = {
  onClickCallback: React.PropTypes.func.isRequired
}

