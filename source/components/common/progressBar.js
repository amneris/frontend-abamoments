import React from 'react';

export default class ProgressBar extends React.Component {

  render() {

    return (
      <div className="progressBar-container">
        <div className="progressBar-inner" style={{width: this.props.percentage + '%'}} />
      </div>
    );
  }
}

ProgressBar.propTypes = {
  percentage: React.PropTypes.number.isRequired
}

