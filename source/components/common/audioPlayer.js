import React from 'react';

export default class AudioPlayer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isReady: undefined,
      isEnded: undefined
    };
  }

  componentWillMount() {
    this.setState({
      isReady: false,
      isEnded: false
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.shouldPlayStart && this.props.shouldPlayStart && this.state.isReady
        || !prevState.isReady && this.state.isReady && this.props.shouldPlayStart) {

      this.play();
    }
  }

  play() {
    this.audioElement.play();
  }

  handleCanPlay() {
    this.setState({
      isReady: true
    });
  }

  handlePlay() {
    if (this.props.onPlayCallback) {
      this.props.onPlayCallback();
    }
  }

  handlePlayEnded() {
    this.setState({
      isEnded: true
    });

    if (this.props.onPlayEndedCallback) {
      this.props.onPlayEndedCallback();
    }
  }

  render() {

    return (
      <audio ref={(ref) => { this.audioElement = ref; }}
             src={this.props.source}
             onCanPlay={::this.handleCanPlay}
             onPlay={::this.handlePlay}
             onEnded={::this.handlePlayEnded} />
    );
  }
}

AudioPlayer.propTypes = {
  onPlayCallback: React.PropTypes.func,
  onPlayEndedCallback: React.PropTypes.func,
  shouldPlayStart: React.PropTypes.bool,
  source: React.PropTypes.string.isRequired
}
