import React from 'react';

export default class Button extends React.Component {

  constructor(props) {
    super(props);

    this.autoClickTimeout = undefined;
  }

  componentDidMount() {
    if (this.props.autoClickTime) {
      this.autoClickTimeout = setTimeout(::this.handleClick, this.props.autoClickTime);

      this.buttonElement.style.animationDuration = `${this.props.autoClickTime}ms`;
    }
  }

  componentWillUnmount() {
    clearTimeout(this.autoClickTimeout);
  }

  handleClick() {
    clearTimeout(this.autoClickTimeout);

    this.props.onClickCallback();
  }

  render() {

    return (
      <div className={`button type-${this.props.type} ${this.props.autoClickTime ? 'autoClick' : ''}`}
           ref={(ref) => { this.buttonElement = ref; }}
           onClick={::this.handleClick}>

        <span className="button-text">{ this.props.text }</span>

      </div>
    );
  }
}

Button.propTypes = {
  onClickCallback: React.PropTypes.func.isRequired,
  autoClickTime: React.PropTypes.number,
  text: React.PropTypes.string.isRequired,
  type: React.PropTypes.oneOf(['basic']).isRequired
}

