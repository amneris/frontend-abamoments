import React from 'react';

export default class BubbleNotification extends React.Component {

  constructor(props) {
    super(props);

    this.icon = undefined;
    this.scaleFactor = undefined;
  }

  componentWillMount() {
    this.scaleFactor = this.props.scaleFactor || 1;

    switch (this.props.type) {
      case 'done':
        this.icon = 'ui-glyph-1_check';
        break;

      case 'new':
        this.icon = 'ui-glyph-2_favourite-31';
        break;

      case 'inactive':
        this.icon = 'ui-glyph-1_lock';
        break;
    }
  }

  render() {
    let divStyle = {
      transform: `scale(${this.scaleFactor})`
    };

    return (
      <div className={`bubbleNotification-container type-${this.props.type} base-icon-${this.icon}`}
           style={divStyle} />
    );
  }
}

BubbleNotification.propTypes = {
  type: React.PropTypes.oneOf(['active', 'inactive', 'new', 'done']).isRequired,
  scaleFactor: React.PropTypes.number
}

