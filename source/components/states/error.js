import React from 'react';

export default class Error extends React.Component {

  componentWillMount() {
    this.errorMessage = this.props.errorMessage || this.context.localeManager.getMessage('label error thrown');
    this.buttonMessage = this.props.buttonMessage || this.context.localeManager.getMessage('label button try again');
  }

  handleClick() {
    this.props.onClickCallback();
  }

  render() {

    return (
      <div className="aba-error">
        <div className="error-container">
          <div className="error-icon base-icon-ui-outline-3_alert"></div>
          <div className="error-text">{this.errorMessage}</div>
          <button className="error-button" type="button" onClick={::this.handleClick}>
            {this.buttonMessage}
          </button>
        </div>
      </div>
    );
  }
}

Error.propTypes = {
  buttonMessage: React.PropTypes.string,
  errorMessage: React.PropTypes.string,
  onClickCallback: React.PropTypes.func.isRequired
}

Error.contextTypes = {
  localeManager: React.PropTypes.object.isRequired
}
