import React from 'react';

import AppConfig from '../../config/appConfig';

import AudioPlayer from '../common/audioPlayer';
import BubbleNotification from '../common/bubbleNotification';

export default class MomentItem extends React.Component {

  constructor(props) {
    super(props);

    this.minimumAnimationTime = 1000;
    this.currentAnimationTime = 0;
    this.currentAnimationTimeInterval = undefined;
    this.currentAnimationTimeNextLoop = 2000;

    this.state = {
      status: undefined,
      shouldPlayDoneAudio: undefined
    };
  }

  componentWillMount() {
    this.currentAnimationTime = 0;

    this.formatProps(this.props);

    this.setState({
      shouldPlayDoneAudio: false
    });
  }

  componentWillUnmount() {
    if (this.currentAnimationTimeInterval) {
      clearInterval(this.currentAnimationTimeInterval);
    }
  }

  componentWillReceiveProps(nextProps) {
    this.formatProps(nextProps);
  }

  componentDidUpdate(prevProps) {
    // handle animation time transition from Active state to Done state
    if (!prevProps.startAnimateMomentDone && this.props.startAnimateMomentDone) {
      // start counting the time of running the animation
      this.currentAnimationTimeInterval = setInterval(() => {
        if (this.currentAnimationTime < this.minimumAnimationTime) {
          this.currentAnimationTime += this.currentAnimationTimeNextLoop;
        }
        else {
          clearInterval(this.currentAnimationTimeInterval);
        }
      }, this.currentAnimationTimeNextLoop);
    }

    // when the progress is saved
    if (this.props.startAnimateMomentDone && this.props.isProgressSaved) {
      // if the animation time is higher than the minimum, call finish function
      if (this.currentAnimationTime >= this.minimumAnimationTime)  {
        this.handleMomentDone();
      }
      else {
        // if not, call finish function after the animation minimum time
        setTimeout(() => {
          this.handleMomentDone();
        }, this.minimumAnimationTime - this.currentAnimationTime);
      }
    }
  }

  formatProps(props) {
    let status = props.status.toLowerCase();

    this.setState({
      status: status
    });
  }

  handleClick() {
    if (this.state.status !== 'inactive') {
      this.props.onClickCallback(this.props.id);
    }
  }

  handleMomentDone() {
    clearInterval(this.currentAnimationTimeInterval);

    this.setState({
      status: 'done',
      shouldPlayDoneAudio: true
    });

    this.props.onMomentDoneCallback();
  }

  render() {

    return (
      <div className={`momentItem status-${this.state.status}`}>

        <div className={`bubble nucleo-icon-${this.props.icon}`}
             onClick={::this.handleClick}>

          {(() => {
            if (this.state.status !== 'active') {
              return <BubbleNotification type={this.state.status} scaleFactor={0.6} />
            }
          })()}

          {(() => {
            if (this.props.startAnimateMomentDone) {
              return <div className="momentItem-animationWrapper" />
            }
          })()}

        </div>

        <AudioPlayer source="/assets/audio/momentDone.mp3" shouldPlayStart={this.state.shouldPlayDoneAudio} />

        <div className="name">{ this.props.name }</div>

      </div>
    );
  }
}

MomentItem.propTypes = {
  id: React.PropTypes.string.isRequired,
  icon: React.PropTypes.string.isRequired,
  isProgressSaved: React.PropTypes.bool.isRequired,
  status: React.PropTypes.oneOf(['Active', 'Inactive', 'New', 'Done']).isRequired,
  name: React.PropTypes.string.isRequired,
  englishName: React.PropTypes.string.isRequired,
  startAnimateMomentDone: React.PropTypes.bool.isRequired,
  onMomentDoneCallback: React.PropTypes.func.isRequired,
  onClickCallback: React.PropTypes.func.isRequired
}
