require('es6-promise').polyfill();
import btoa from 'btoa'; // polyfill

import fetch from 'isomorphic-fetch';
import ClientOAuth2 from 'client-oauth2';
import QueryString from 'query-string';

import AppConfig from '../config/appConfig';

export function checkStatus(response) {

  let status = response.status;

  switch (true) {
    case (status >= 200 && status < 300):
      return response;
    default:
      var error = new Error(response.statusText);
      error.status = status;
      error.response = response;
      throw error;
  }
}

/**
 *
 * @param url
 * @param method (get or post)
 * @param params (when method is post)
 * @param includeCredentials (true when api is abawebapps)
 * @returns {Promise.<T>}
 */
export function apiRequest(url, method = 'get', opts = {}) {

  var defaultOpts = {
    mode: 'cors',
    cache: 'no-cache',
    method: method
  };

  if (method === 'post') {
    opts.headers = Object.assign({ 'Content-Type' : 'application/x-www-form-urlencoded' }, opts.headers);
    opts.body = QueryString.stringify(opts.body);
  }

  opts = Object.assign(defaultOpts, opts);

  return fetch(url, opts)
    .then(checkStatus)
    .then(apiResponse => apiResponse.json());
}

export function apiTokenRequestByPassword(authUrl, username, password) {
  const authorization = new ClientOAuth2({
    clientId: AppConfig.getOauth2ClientId(),
    clientSecret: AppConfig.getOauth2ClientSecret(),
    accessTokenUri: authUrl
  });

  return Promise.resolve(ClientOAuth2GetToken(authorization, username, password));
}

export function apiTokenRequestByAutoLoginKey(authUrl, username, autoLoginKey) {
  const clientId = AppConfig.getOauth2ClientId();
  const clientSecret = AppConfig.getOauth2ClientSecret();

  const authorization = new ClientOAuth2({
    clientId: clientId,
    clientSecret: clientSecret,
    accessTokenUri: authUrl
  });

  return apiRequest(authUrl, 'post', {
    headers: {
      Authorization: 'Basic ' + btoa(clientId + ':' + clientSecret)
    },
    body: {
      grant_type: 'token',
      username: username,
      password: autoLoginKey
    }
  }).then((authResponse) => {
    // manually create a token instance from the oauth library
    let token = ClientOAuth2CreateToken(authorization, authResponse);

    return token;
  });
}

export function apiSecureRequest(authToken, url, method = 'get', params = {}) {

  let fetchParams = {
    mode: 'cors',
    cache: 'no-cache',
    method: method
  };

  let authParams = authToken.sign({
    method: method,
    url: url
  });

  fetchParams = Object.assign(fetchParams, authParams);

  if (method === 'post') {
    fetchParams.headers = Object.assign({}, fetchParams.headers, {
      'Content-Type' : 'application/json;charset=UTF-8'
    });

    fetchParams.body = JSON.stringify(params);
  }

  return fetch(url, fetchParams)
    .then(checkStatus)
    .then(apiResponse => apiResponse.json());
}

export function staticGet(url) {

  var fetchParams = {
    mode: 'cors',
    cache: 'default'
  };

  return fetch(url, fetchParams)
    .then(checkStatus)
    .then(response => response.text());
}

export function ClientOAuth2GetToken(authorization, username, password) {
  return authorization.owner.getToken(username, password);
}

export function ClientOAuth2CreateToken(authorization, authResponse) {
  let token = authorization.createToken(authResponse.access_token, authResponse.refresh_token, authResponse.token_type, authResponse);
  token.expiresIn(authResponse.expires_in);

  return token;
}

export function ClientOAuth2HasExpired(authorization) {
  return authorization.expired();
}
export function ClientOAuth2RefreshToken(authorization) {
  return authorization.refresh();
}