import React from 'react';
import { Route, IndexRedirect, Redirect } from 'react-router';

import App from './containers/app';
import Dashboard from './containers/pages/dashboard';
import Moment from './containers/pages/moment';

export const PAGES_ROUTES = {
  DASHBOARD : '/dashboard',
  MOMENT: '/moment'
};

export const DEFAULT_PAGE = PAGES_ROUTES.DASHBOARD;
export const DEFAULT_LOCALE = 'en';

export default (
  <div>
    <Route component={App}>
      <IndexRedirect to={'/:lang' + PAGES_ROUTES.DASHBOARD}/>
      <Route path={'/:lang' + PAGES_ROUTES.DASHBOARD} component={Dashboard}/>
      <Route path={'/:lang' + PAGES_ROUTES.MOMENT} component={Moment}/>
    </Route>
    <Redirect from="*" to={DEFAULT_LOCALE + '/' + DEFAULT_PAGE}/>
  </div>
);
