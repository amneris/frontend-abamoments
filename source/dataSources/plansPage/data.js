/**
 * populate an array which contains hardcoded user features and reviews
 * @type {Array}
 */

var dataSources = []
dataSources['features'] = require('./features.json');

module.exports = dataSources;
