import { getDefaultLocale } from '../reducers/utils';

/**
 * class responsible for getting application strings according to user's locale
 */
export default class LocaleManager {
  constructor(locale) {

    this.messages = [];

    if (!locale) {
      locale = getDefaultLocale();
    }

    if (locale) {
      this.setMessages(locale)
    };
  }

  /**
   * method that converts a locales messages specified in PO to the format that
   * the FormatJS library expects
   * @param locale
   */
  setMessages(locale) {

    var localeMessages = require('./phrases/' + locale + '.json');

    this.messages = localeMessages;
  }

  /**
   * returns a message for a given key
   * @param key, values
   * @returns {message for key, or key if value not found}
   */
  getMessage(key, values) {
    if (this.messages[key] === undefined) {
      return key;
    }
    else if (values !== undefined) {
      var message = this.messages[key];
      for (var prop in values) {
        if (values.hasOwnProperty(prop)) {
          message = message.replace('{' + prop + '}', values[prop]);
        }
      }

      return message;
    }

    return this.messages[key];
  }
}
