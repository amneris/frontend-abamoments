import { SET_NEXT_UNIT, SET_TEACHER } from '../actions/actionTypes';

export function course(state = {
  nextUnit: {
    id: null
  },
  teacher: {
    image: null
  }
}, action) {

  switch (action.type) {

    case SET_NEXT_UNIT:

      return Object.assign({}, state, {
        nextUnit: {
          id: action.nextUnit.id
        }
      });

    case SET_TEACHER:

      return Object.assign({}, state, {
        teacher: {
          image: action.teacher.image
        }
      });

    default:

      return state;
  }
}