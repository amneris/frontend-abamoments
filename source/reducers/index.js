import { combineReducers } from 'redux';

import { apiAuth } from './apiAuth';
import { appState } from './appState';
import { user } from './user';
import { course } from './course';

const rootReducer = combineReducers({
  apiAuth, appState, user, course
});

export default rootReducer;
