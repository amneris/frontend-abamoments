import expect from 'expect'

import {
  SET_APP_STATE, SELECT_PLAN
} from '../actions/actionTypes';

import { appState, appStateInit }
  from './appState';

describe('appState reducer', () => {
  it('should return the initial state', () => {
    expect(
      appState(undefined, {})
    ).toEqual(appStateInit);
  });

  it('should handle SET_APP_STATE', () => {
    expect(
      appState({}, {
        type: SET_APP_STATE,
        appState: {
          title: 'Hello World',
          language: 'en'
        }
      })
    ).toEqual({
      title: 'Hello World',
      language: 'en'
    });
  });
});
