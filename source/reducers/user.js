import { SET_USER } from '../actions/actionTypes';

export function user(state = {
  autoLoginKey: null,
  country: null,
  email: null,
  id: null,
  name: null,
  password: null,
  surname: null,
  userType: null
}, action) {

  switch (action.type) {

    case SET_USER:
      return Object.assign({}, state, action.user);

    default:
      return state;
  }
}