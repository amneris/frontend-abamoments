import { SET_API_AUTH } from '../actions/actionTypes';

export const apiAuthInit = {
  authToken: null,
  accessToken: null,
  expires: null,
  refreshToken: null,
  tokenType: null,
  userType: 0,
  userId: 0
};

export function apiAuth(state = apiAuthInit, action) {

  switch (action.type) {
    case SET_API_AUTH:
      return Object.assign({}, state,
        {
          authToken: action.authToken
        }
      );

    default:
      return state;
  }
}
