import { getDefaultContactPhone } from './utils';
import * as ActionTypes from '../actions/actionTypes';

export const appStateInit = {
  language: null,
  urlParams: {},
  title: null,
  currentMoment: null,
  isEndingMoment: false,
  isProgressSaved: false
};

export function appState(state = appStateInit, action) {

  switch (action.type) {
    case ActionTypes.SET_APP_STATE:
      return Object.assign({}, state,
        action.appState
      );

    case ActionTypes.SET_URL_PARAMS:
      return Object.assign({}, state, {
          urlParams: action.urlParams
        }
      );

    case ActionTypes.GO_TO_MOMENT:
      return Object.assign({}, state, {
          currentMoment: action.moment,
          isEndingMoment: false,
          isProgressSaved: false
        }
      );

    case ActionTypes.START_FINISH_MOMENT:
      return Object.assign({}, state, {
          isEndingMoment: true
        }
      );

    case ActionTypes.END_FINISH_MOMENT:
    case ActionTypes.CLOSE_MOMENT:
      return Object.assign({}, state, {
          currentMoment: null,
          isEndingMoment: false
        }
      );

    case ActionTypes.SET_PROGRESS_SAVED:
      return Object.assign({}, state, {
          isProgressSaved: true
        }
      );

    default:
      return state;
  }
}
