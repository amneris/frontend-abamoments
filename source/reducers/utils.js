export const DEFAULT_LANGUAGE = 'en';
export const DEFAULT_LOCALE = 'en-EN';
const SUPPORTED_LANGUAGES = ['de', 'en', 'es', 'fr', 'it', 'pt', 'ru'];
export const SUPPORTED_LOCALES = ['de-DE', 'en-EN', 'es-ES', 'fr-FR', 'it-IT', 'pt-PT', 'ru-RU'];
const PATH_SEPARATOR = '/';


// returns a valid locale to load the app
export function convertLanguageToLocale(language) {
  language = language || getDefaultLanguage();

  if (language.length === 2) {
    for (var i = 0; i < SUPPORTED_LOCALES.length; i++) {
      if (startsWith(SUPPORTED_LOCALES[i],language)) {
        return SUPPORTED_LOCALES[i];
      }
    }
  }

  return getDefaultLocale();
}

export function getLanguageFromUrl(pathname = PATH_SEPARATOR) {
  if (pathname) {
    const urlParts = pathname.split(PATH_SEPARATOR);

    if (urlParts.length > 1) {
      var found = SUPPORTED_LANGUAGES.find(x => x === urlParts[1]);

      return found || getDefaultLanguage();
    }
  }

  return getDefaultLanguage();
}

export function getDefaultLanguage() {
  return DEFAULT_LANGUAGE;
}

export function getDefaultLocale() {
  return DEFAULT_LOCALE;
}

export function startsWith(origin,str) {
  if (!str) return false;
  if (!String.prototype.startsWith) {
    //return (origin.indexOf(str) === 0);
    return (origin.lastIndexOf(str, 0) === 0);
  } else {
    return origin.startsWith(str);
  }
}

