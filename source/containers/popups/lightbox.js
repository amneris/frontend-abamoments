import React from 'react';

import Button from '../../components/common/button';
import CloseButton from '../../components/common/closeButton';

export default class Lightbox extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isVisible: undefined
    };
  }

  componentWillMount() {
    this.setState({
      isVisible: true
    });
  }

  handleClose() {
    this.setState({
      isVisible: false
    });

    this.props.onCloseCallback();
  }

  handleClick() {
    this.props.onContinueCallback();
  }

  render() {

    if (this.state.isVisible) {

      return (
        <div className="lightbox">
          <div className="lightbox-wrapper">
            <CloseButton onClickCallback={::this.handleClose}/>

            <div className="lightbox-children">
              {this.props.children}
            </div>

            <Button onClickCallback={::this.handleClick}
                    {...this.props.autoContinueTime ? {autoClickTime: this.props.autoContinueTime} : {}}
                    text={this.props.buttonText}
                    type="basic"/>
          </div>
        </div>
      );
    }
    else {

      return false;
    }
  }
}

Lightbox.propTypes = {
  autoContinueTime: React.PropTypes.number,
  buttonText: React.PropTypes.string.isRequired,
  onCloseCallback: React.PropTypes.func.isRequired,
  onContinueCallback: React.PropTypes.func.isRequired
}
