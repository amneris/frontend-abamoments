import React from 'react';

export default class MomentEnd extends React.Component {

  render() {

    return (
      <div className="momentEnd">
        <img className="momentEnd-teacherImage" src={this.props.teacherImage} />
        <div className="momentEnd-title">
          {this.context.localeManager.getMessage('end-popup-title')}
        </div>
        <div className="momentEnd-info">
          {this.context.localeManager.getMessage('end-popup-info')}
        </div>
      </div>
    );
  }
}

MomentEnd.propTypes = {
  teacherImage: React.PropTypes.string.isRequired
}

MomentEnd.contextTypes = {
  localeManager: React.PropTypes.object
}
