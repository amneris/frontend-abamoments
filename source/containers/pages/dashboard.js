import React from 'react';
import { connect } from 'react-redux';



import { callActionViewDashboard, callActionGoToMoment, callActionEndFinishMoment, callActionGoToCourse } from '../../actions/synch';
import MomentsList from './dashboard/momentsList';
import Lightbox from '../popups/lightbox';
import MomentEnd from '../popups/partials/momentEnd';


class Dashboard extends React.Component {

  constructor(props) {
    super(props);

    this.momentType = 'vocabulary'; // todo this will come from the future previous page

    this.state = {
      isLoaded: undefined,
      selectedMoment: undefined,
      showEndingMomentPopup: undefined
    };
  }

  componentWillMount() {
    this.setState({
      isLoaded: false,
      selectedMoment: null,
      showEndingMomentPopup: false
    });
  }

  componentDidMount() {
    this.props.callActionViewDashboard(this.momentType);
  }

  handleSelectMoment(moment) {
    this.props.callActionGoToMoment(moment);
  }

  handleMomentDone() {
    setTimeout(() => {
      this.setState({
        showEndingMomentPopup: true
      });
    }, 2500);
  }

  handleCloseEndPopup() {
    this.props.callActionEndFinishMoment();

    this.setState({
      showEndingMomentPopup: false
    });
  }

  handleGoToNextUnit() {
    this.props.callActionGoToCourse(this.props.nextUnit.id);
  }

  render() {

    return (
      <div className="dashboard-container">

        <div className="category-header">

          <div className="category-title">
            <span className="category-title-aba">ABA</span>moments<sup>®</sup>
          </div>

          <div className="category-description">
            {this.context.localeManager.getMessage('category-vocabulary-description')}
          </div>

          <div className="category-propertiesList">
            <div className="category-property">
              <span className="category-icon base-icon-text-glyph_caps-small"/>
              <span className="category-name">
                {this.context.localeManager.getMessage('category-vocabulary-name')}
              </span>
            </div>
            <div className="category-property">
              <span className="category-icon base-icon-ui-outline-2_time"/>
              <span className="category-name" data-minutes="1">
                {this.context.localeManager.getMessage('category-minutes', { minutes: 1 })}
              </span>
            </div>
          </div>

        </div>

        {(() => {
          if (this.state.showEndingMomentPopup && this.props.nextUnit.id && this.props.teacher.image) {

            return (
              <Lightbox onCloseCallback={::this.handleCloseEndPopup}
                        onContinueCallback={::this.handleGoToNextUnit}
                        autoContinueTime={7000}
                        buttonText={this.context.localeManager.getMessage('button-next-unit', { unit: this.props.nextUnit.id })}
                        >

                <MomentEnd teacherImage={this.props.teacher.image} unitId={this.props.nextUnit.id} />

              </Lightbox>
            );
          }
        })()}

        <MomentsList onSelectMomentCallback={::this.handleSelectMoment}
                     onMomentDoneCallback={::this.handleMomentDone} />

      </div>
    );
  }
}

Dashboard.contextTypes = {
  localeManager: React.PropTypes.object
}

function mapStateToProps(state) {
  const { isEndingMoment } = state.appState;
  const { nextUnit, teacher } = state.course;

  return {
    isEndingMoment: isEndingMoment,
    nextUnit,
    teacher
  }
}

export default connect(
  mapStateToProps,
  { callActionViewDashboard, callActionGoToMoment, callActionEndFinishMoment, callActionGoToCourse }
)(Dashboard);
