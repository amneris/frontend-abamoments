import React from 'react';
import { connect } from 'react-redux';

import { callActionFetchMomentsList } from '../../../actions/asynch';
import Loading from '../../../components/states/loading';
import Error from '../../../components/states/error';
import MomentItem from '../../../components/dashboard/momentItem';

class MomentsList extends React.Component {

  constructor(props) {
    super(props);

    this.moments = undefined;

    this.state = {
      isLoaded: undefined,
      startAnimateMomentDone: undefined
    };
  }

  componentWillMount() {
    this.moments = [];

    this.setState({
      isLoaded: undefined, // false value will be set in case of error
      startAnimateMomentDone: false
    });
  }

  componentDidMount() {
    this.fetchMomentsList();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.isLoaded && this.state.isLoaded && this.props.isEndingMoment) {
      this.setState({
        startAnimateMomentDone: true
      });
    }
  }

  fetchMomentsList() {
    this.setState({
      isLoaded: undefined
    });

    this.props.callActionFetchMomentsList
      .call(this)
      .then(momentsResponse => {
        this.moments = momentsResponse;

        this.moments = this.moments.map((moment) => {
          let title = moment.titles.find(title => title.language === this.props.language);
          let englishTitle = moment.titles.find(title => title.language === 'en');

          moment.type = moment.momentType;
          moment.name = title.name;
          moment.englishName = englishTitle.name;
          delete(moment.titles);

          return moment;
        });

        this.setState({
          isLoaded: true
        });
      })
      .catch(() => {
        this.setState({
          isLoaded: false
        });
      });
  }

  handleSelectMoment(id) {
    let selectedMoment = this.moments.find(moment => moment.id === id);

    this.props.onSelectMomentCallback(selectedMoment);
  }

  handleMomentDone() {
    this.setState({
      startAnimateMomentDone: false
    });

    this.props.onMomentDoneCallback();
  }

  render() {

    return (
      <div className="momentsList-container">
        {(() => {
          if (this.state.isLoaded === true) {
            let output = [];

            this.moments.map((moment, index) => {
              let offsetClass = index % 5 === 0 ? 'col-xs-offset-1 col-sm-offset-1' : '';
              let startAnimateMomentDone = false;
              let isProgressSaved = false;

              if (this.props.lastMomentDone && moment.id === this.props.lastMomentDone.id) {
                startAnimateMomentDone = this.state.startAnimateMomentDone;
                isProgressSaved = this.props.isProgressSaved;

                if (startAnimateMomentDone) moment.status = 'Active';
                else moment.status = 'Done';
              }

              output.push(
                <div key={`moment:${index}:${moment.id}`}
                     id={`moment:${index}:${moment.id}`}
                     className={`moment-wrapper col-xs-2 col-sm-2 ${offsetClass}`}>

                  <MomentItem id={moment.id}
                              icon={moment.icon}
                              isProgressSaved={isProgressSaved}
                              status={moment.status}
                              name={moment.name}
                              englishName={moment.englishName}
                              startAnimateMomentDone={startAnimateMomentDone}
                              onMomentDoneCallback={::this.handleMomentDone}
                              onClickCallback={::this.handleSelectMoment}/>
                </div>
              );
            });

            return output;
          }
          else if (this.state.isLoaded === false) {

            return <Error onClickCallback={::this.fetchMomentsList}
                          errorMessage={this.context.localeManager.getMessage('generic-error-message')}
                          buttonMessage={this.context.localeManager.getMessage('generic-error-message-retry')} />
          }
          else {

            return <Loading />
          }
        })()}

        <div className="clearer" />
      </div>
    );
  }
}

MomentsList.contextTypes = {
  localeManager: React.PropTypes.object
}

MomentsList.propTypes = {
  onMomentDoneCallback: React.PropTypes.func.isRequired,
  onSelectMomentCallback: React.PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { isEndingMoment, isProgressSaved, currentMoment, language } = state.appState;

  return {
    lastMomentDone: currentMoment,
    isEndingMoment,
    isProgressSaved,
    language
  };
}

export default connect(
  mapStateToProps,
  { callActionFetchMomentsList }
)(MomentsList);
