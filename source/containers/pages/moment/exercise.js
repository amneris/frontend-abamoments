import React from 'react';
import { connect } from 'react-redux';
import shuffle from 'shuffle-array';

import { callActionStartMomentExercise, callActionSelectMomentAnswer } from '../../../actions/synch';

import Question from '../../../components/exercise/question';
import Answer from '../../../components/exercise/answer';

class Exercise extends React.Component {

  constructor(props) {
    super(props);

    let data = props.data;

    this.id = data.id;
    this.type = data.type;
    this.question = data.items.find(question => question.role === 'question');
    this.answers = data.items.filter(answer => answer.role === 'answer');
    this.correctAnswerId = data.results[0].id;

    this.onFinishExerciseCallback = props.onFinishExerciseCallback;

    this.state = {
      lastAnswerClicked: undefined,
      isAnsweredCorrectly: undefined
    }
  }

  componentWillMount() {
    shuffle(this.answers);

    this.setState({
      isAnsweredCorrectly: undefined
    });
  }

  componentDidMount() {
    if (this.props.isCurrentExercise) {
      this.handleCurrentExercise();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isCurrentExercise && nextProps.isCurrentExercise) {
      this.handleCurrentExercise();
    }
  }

  handleCurrentExercise() {
    this.props.callActionStartMomentExercise(this.props.momentId, this.props.momentType, this.id, this.type);
  }

  handleAnswer(answerId, isCorrect) {
    this.props.callActionSelectMomentAnswer(this.props.momentId, this.props.momentType, this.id, this.type, answerId, isCorrect);

    this.setState({
      lastAnswerClicked: answerId,
      isAnsweredCorrectly: isCorrect
    });
  }

  render() {

    return (
      <div className="exercise-container">

        <div id={`question:${this.question.id}`} className="question-wrapper">
          <Question momentId={this.props.momentId}
                    itemType={this.question.type}
                    content={this.question.value}
                    audio={this.question.audio}
                    lastAnswerClicked={this.state.lastAnswerClicked}
                    isAnsweredCorrectly={this.state.isAnsweredCorrectly}
                    onFinishExerciseCallback={this.onFinishExerciseCallback} />
        </div>

        <div className="answerList-wrapper">
          {(() => {
            let output = [];

            for (let i = 0; i < this.answers.length; i ++) {
              output.push(
                <div key={`answer:${i}:${this.answers[i].id}`} id={`answer:${i}:${this.answers[i].id}`} className="answer-wrapper">
                  <Answer momentId={this.props.momentId}
                          id={this.answers[i].id}
                          itemType={this.answers[i].type}
                          isCorrect={this.answers[i].id === this.correctAnswerId}
                          content={this.answers[i].value}
                          onClickCallback={this.handleAnswer.bind(this)}
                          isAnsweredCorrectly={this.state.isAnsweredCorrectly} />
                </div>
              );
            }

            return output;
          })()}
        </div>

      </div>
    );
  }
}

Exercise.propTypes = {
  data: React.PropTypes.object.isRequired,
  isCurrentExercise: React.PropTypes.bool.isRequired,
  momentId: React.PropTypes.string.isRequired,
  momentType: React.PropTypes.string.isRequired
}

function mapStateToProps(state) {

  return {};
}

export default connect(
  mapStateToProps,
  { callActionStartMomentExercise, callActionSelectMomentAnswer }
)(Exercise);
