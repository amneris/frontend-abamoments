import React from 'react';
import { connect } from 'react-redux';
import shuffle from 'shuffle-array';

import AppConfig from '../../config/appConfig';

import { callActionGoToDashboard, callActionStartMoment, callActionStartFinishMoment, callActionCloseMoment } from '../../actions/synch';
import { callActionFetchMoment, callActionSaveMomentDone } from '../../actions/asynch';
import Loading from '../../components/states/loading';
import Error from '../../components/states/error';
import ProgressBar from '../../components/common/progressBar';
import CloseButton from '../../components/common/closeButton';
import Exercise from './moment/exercise';
import Countdown from '../../components/exercise/countdown';

class Moment extends React.Component {

  constructor(props) {
    super(props);

    this.exercises = undefined;

    this.state = {
      isLoaded: undefined,
      isStarted: undefined,
      currentExercise: undefined,
      progress: undefined
    };
  }

  componentWillMount() {
    this.exercises = [];

    this.setState({
      isLoaded: undefined,
      isStarted: false,
      currentExercise: 0,
      progress: 0
    });

    window.addEventListener('beforeunload', this.preventPageRefresh);
  }

  componentDidMount() {
    if (this.props.id) {
      this.props.callActionFetchMoment
        .call(this, this.props.id)
        .then(momentResponse => {
          this.exercises = momentResponse.exercises;
          shuffle(this.exercises);

          this.setState({
            isLoaded: true
          });
        })
        .catch(() => {
          this.setState({
            isLoaded: false
          });
        });
    }
    else {
      this.props.callActionGoToDashboard();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.preventPageRefresh);
  }

  componentDidUpdate() {
    if (this.state.progress === 100) {
      setTimeout(::this.handleFinishMoment, 1000);
    }
  }

  preventPageRefresh(e) {
    var confirmationMessage = 'some custom message';

    e.returnValue = confirmationMessage;
    return confirmationMessage;
  }

  handleStartMoment() {
    this.props.callActionStartMoment(this.props.id, this.props.type);

    this.setState({
      isStarted: true
    });
  }

  handleFinishExercise() {
    let currentExercise = this.state.currentExercise;
    let correctRounds = currentExercise + 1;

    if (currentExercise < this.exercises.length - 1) {
        currentExercise = currentExercise + 1;
    }

    this.setState({
      currentExercise: currentExercise,
      progress:  correctRounds / this.exercises.length * 100
    });
  }

  handleFinishMoment() {
    if (this.props.status !== 'done') {
      this.props.callActionSaveMomentDone(this.props.id);
      this.props.callActionStartFinishMoment(this.props.id, this.props.type);
    }
    this.props.callActionGoToDashboard();
  }

  handleCloseMoment() {
    this.props.callActionCloseMoment(this.props.id, this.props.type);
    this.props.callActionGoToDashboard();
  }

  render() {

    return (
      <div className="moment-container">

        <CloseButton onClickCallback={::this.handleCloseMoment}/>

        {(() => {
          if (this.state.isLoaded === true) {
            let output = [];

            output.push(<ProgressBar key='progress-bar' percentage={this.state.progress} />);

            for (let i = 0; i < this.exercises.length; i++) {
              output.push(
                <div key={`exercise:${i}:${this.exercises[i].id}`} id={`exercise:${i}:${this.exercises[i].id}`}
                     className={`exercise-wrapper ${i === this.state.currentExercise ? 'currentExercise' : 'hiddenExercise'}`}>

                  <Exercise momentId={this.props.id}
                            momentType={this.props.type}
                            data={this.exercises[i]}
                            isCurrentExercise={this.state.isStarted && i === this.state.currentExercise}
                            onFinishExerciseCallback={this.handleFinishExercise.bind(this)}/>
                </div>
              );
            }

            return output;
          }
          else if (this.state.isLoaded === false) {
            return <Error onClickCallback={this.props.callActionGoToDashboard}
                          errorMessage={this.context.localeManager.getMessage('generic-error-message')}
                          buttonMessage={this.context.localeManager.getMessage('generic-error-message-retry')} />
          }
          else {
            return <Loading />
          }
        })()}

        {(() => {
          if (!this.state.isStarted) {

            return <Countdown momentAudio={`${AppConfig.getAudioAssetsUrl()}/${this.props.id}/${this.props.audio}.mp3`}
                              momentName={this.props.englishName}
                              onFinishCallback={::this.handleStartMoment}/>
          }
        })()}

      </div>
    );
  }
}

Moment.contextTypes = {
  localeManager: React.PropTypes.object
}

Moment.propTypes = {
  audio: React.PropTypes.string.isRequired,
  id: React.PropTypes.string.isRequired,
  englishName: React.PropTypes.string.isRequired,
  type: React.PropTypes.string.isRequired,
  status: React.PropTypes.string.isRequired
}

function mapStateToProps(state) {
  const { currentMoment } = state.appState;

  if (currentMoment) {
    return {
      audio: currentMoment.audio,
      id: currentMoment.id,
      englishName: currentMoment.englishName,
      type: currentMoment.type,
      status: currentMoment.status
    };
  }

  return {};
}

export default connect(
  mapStateToProps,
  {
    callActionGoToDashboard,
    callActionFetchMoment,
    callActionStartMoment,
    callActionStartFinishMoment,
    callActionSaveMomentDone,
    callActionCloseMoment
  }
)(Moment);
