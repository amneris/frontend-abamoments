import { browserHistory } from 'react-router';
import QueryString from 'query-string';

import { PAGES_ROUTES } from '../../routes';
import AppConfig from '../../config/appConfig';
import * as actionTypes from '../../actions/actionTypes';

const urlManager = store => next => action => {

  const state = store.getState();

  switch (action.type) {

    case actionTypes.FORBID_ACCESS:
      //goToCampusLogin();
      break;

    case actionTypes.GO_TO_DASHBOARD:
      goToPage(state, PAGES_ROUTES.DASHBOARD);
      break;

    case actionTypes.GO_TO_MOMENT:
      goToPage(state, PAGES_ROUTES.MOMENT);
      break;

    case actionTypes.GO_TO_COURSE:
      goToCourse(state, action.unitId);
      break;
  }

  return next(action);
}

function goToCampusLogin() {
  let url = AppConfig.getCampusUrl() + '/login';
  let params = {
    'returnTo' : encodeURIComponent(window.location.href)
  };

  if (Object.keys(params).length > 0) url += '?' + QueryString.stringify(params);

  window.location = url;
}

function goToPage(state, page) {
  let uri = '/' + state.appState.language + page;
  let params = state.appState.urlParams;

  if (Object.keys(params).length > 0) uri += '?' + QueryString.stringify(params);

  browserHistory.push(uri);
}

function goToCourse(state, unitId) {
  let url = `${AppConfig.getCampusUrl()}/${state.appState.language}/course/AllUnits`;

  if (unitId) {
    url += `/unit/${('00' + unitId).slice(-3)}`;
  }
  else {
    url += '/index';
  }

  redirectToExternalSite(url);
}

function redirectToExternalSite(url) {
  if (window.location !== window.parent.location) {
    // site is inside an iFrame so we execute the redirect on the parent
    window.parent.location = url;
  }
  else {
    window.location = url;
  }
}

export default urlManager;
