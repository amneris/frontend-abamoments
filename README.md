# ABA Moments

## Requirements

* [node](https://nodejs.org/en/)
* gulp: ```sudo npm install -g gulp```

## To run the project

### 1. Add remote repository of project seed

Fork from our [React seed project](https://github.com/abaenglish/aba-react-redux-seed)

After cloning this project run ```git remote add seed git@github.com:abaenglish/aba-react-redux-seed.git```

Then you will be able to update the project seed using ```git pull seed master```

### 2. Install npm packages

Add a file called ```.npmrc``` into your user's folder and paste this code:
```
registry=https://nexus.aba.land:8443/repository/npm-all/
always-auth=true
_auth=<token>
email=<email>
```
_NOTE: Ask the team for the credentials._

Then you will be able to run ```npm install```

### 3. Run the application

Run ```npm run buildAndStart``` the first time you run the application and also every time you want to update the config file.

Run ```gulp``` to rebuild and run the application.

## Overwrite environment variables

You can overwrite the environment variables editing the file called ```env.json``` and the running the application using ```gulp```

