var gulp = require('gulp');
var gulpif = require('gulp-if');
var buffer = require('vinyl-buffer');
var imagemin = require('gulp-imagemin');
var rev = require('gulp-rev');

module.exports = function() {

  return gulp.src('source/assets/img/*.*')
    .pipe(gulpif(!this.opts.isLocal, buffer()))
    //.pipe(gulpif(!this.opts.isLocal, imagemin()))
    .pipe(gulpif(!this.opts.isLocal,rev()))
    .pipe(gulp.dest('dist/assets/img'))
    .pipe(gulpif(!this.opts.isLocal, rev.manifest('dist/rev-manifest.json', {base: process.cwd()+'/dist', merge: true})))
    .pipe(gulpif(!this.opts.isLocal, gulp.dest('dist')));
};
