var gulp = require('gulp');
var isProduction = process.env.NODE_ENV === 'production';
var webpackConfig = isProduction ? require('./../webpack.config.js') :  require('./../webpack-dev.config.js');
var sourcemaps = require('gulp-sourcemaps');
var stream = require('webpack-stream');
var rev = require('gulp-rev');
var gulpif = require('gulp-if');
var livereload = require('gulp-livereload');


var path = {
  ALL: ['source/client.js', 'source/**/*.jsx', 'source/**/*.js'],
  DEST_BUILD_JS: 'dist/assets/scripts'
};

module.exports = function() {

  return gulp.src(path.ALL)
    .pipe(stream(webpackConfig))
    .pipe(gulpif(!this.opts.isLocal, rev()))
    .pipe(gulp.dest(path.DEST_BUILD_JS))
    .pipe(gulpif(!this.opts.isLocal, rev.manifest('dist/rev-manifest.json', {base: process.cwd()+'/dist', merge: true})))
    .pipe(gulpif(!this.opts.isLocal, gulp.dest('dist')))
    .pipe(livereload());
};
