var gulp = require('gulp');
var gulpif = require('gulp-if');
var spritesmith = require('gulp.spritesmith-multi');
var buffer = require('vinyl-buffer');
var imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');
var cssnano = require('gulp-cssnano');
var merge = require('merge-stream');
var rev = require('gulp-rev');


module.exports = function() {
  var destPath = 'dist/assets/stylesheets';
  var spriteData = gulp.src('source/assets/sprite/**/*.png')
    .pipe(spritesmith());

  var imgStream = spriteData.img
    .pipe(buffer())
    /*.pipe(gulpif(!this.opts.isLocal, imagemin({
      optimizationLevel: 3
    })))*/
    .pipe(gulpif(!this.opts.isLocal, rev()))
    .pipe(gulp.dest(destPath))
    .pipe(gulpif(!this.opts.isLocal, rev.manifest('dist/rev-manifest.json', {base: process.cwd()+'/dist', merge: true})) )
    .pipe(gulpif(!this.opts.isLocal, gulp.dest('dist')));

  var cssStream = spriteData.css
    .pipe(concat('sprite.css'))
    .pipe(gulpif(!this.opts.isLocal, cssnano()))
    .pipe(gulpif(!this.opts.isLocal, rev())) 
    .pipe(gulp.dest(destPath))
    .pipe(gulpif(!this.opts.isLocal, rev.manifest('dist/rev-manifest.json', {base: process.cwd()+'/dist', merge: true})) )
    .pipe(gulpif(!this.opts.isLocal, gulp.dest('dist'))); 

  return merge(imgStream, cssStream);
};
