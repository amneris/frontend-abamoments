var gulp = require('gulp');
var replace = require('gulp-replace');
var merge = require('merge-stream');

module.exports = function() {
  var robots = gulp.src('source/robots.txt')
    .pipe(gulp.dest('dist'));

  var errors = gulp.src(['source/403.html', 'source/404.html'])
    .pipe(gulp.dest('dist'));

  return merge(robots, errors);
};
