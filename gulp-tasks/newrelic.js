var gulp = require('gulp');

module.exports = function() {
  return gulp.src('newrelic.js')
    .pipe(gulp.dest('dist'));
};
