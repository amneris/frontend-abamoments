var gulp = require('gulp');

module.exports = function() {

  return gulp.src('source/assets/audio/*.*')
    .pipe(gulp.dest('dist/assets/audio'));
};
