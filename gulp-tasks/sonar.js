var gulp = require('gulp');
var gutil = require('gulp-util');
var sonar = require('gulp-sonar');
var fs = require('fs');

module.exports = function() {

  var pkg = getPackageJson();

  var options = {
    sonar: {
      host: {
        url: 'https://sonar.aba.land/'
      },
      projectKey: 'payment-funnel-v5',
      projectName: 'Payment Funnel',
      projectVersion: pkg.version,
      // comma-delimited string of source directories
      sources: 'source',
      exclusions: 'source/**/*-test.js',
      language: 'js',
      sourceEncoding: 'UTF-8',
      javascript: {
        lcov: {
          reportPath: 'reports/coverage/lcov.info'
        }
      },
      exec: {
        // All these properties will be send to the child_process.exec method (see: https://nodejs.org/api/child_process.html#child_process_child_process_exec_command_options_callback )
        // Increase the amount of data allowed on stdout or stderr (if this value is exceeded then the child process is killed, and the gulp-sonar will fail).
        maxBuffer : 1024*1024
      }
    }
  };

  // gulp source doesn't matter, all files are referenced in options object above
  return gulp.src('thisFileDoesNotExist.js', { read: false })
    .pipe(sonar(options))
    .on('error', function(err) {
      gutil.log(err);
    });
};

function getPackageJson () {
  return JSON.parse(fs.readFileSync('package.json', 'utf8'));
};
